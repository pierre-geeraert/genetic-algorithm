# Projet-DATA branch dev_pierre

We have functionnal GA from internet

To execute this algorithm:

create a virtualenv:
`python3.7 -m venv .env`

connect this virtualenv:
`source .env/bin/activate`

install requirements:
`pip install -r requirements.txt`

execute code:
`python genetic.py`

exit this virtualenv:
`deactivate`
